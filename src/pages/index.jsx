import React, { useState, useCallback} from 'react';
import FormDropdownNumber from '../component/FormDropdownNumber';
import { initialStudentEvaluation, studentsName } from '../features/student';
import './style.css';

const MemoizedForm = React.memo(FormDropdownNumber)

const Student = () => {
    const [showResult, setShowResult] = useState(false)
    const [result, setResult] = useState(initialStudentEvaluation)
    console.log('parent rendered')
    const handleOnSubmit = (e) => {
        e.preventDefault()
        setShowResult(true)
        console.log(result)
    }

    const handleOnchangeValueForm = useCallback((e) => {
        // e.target.name.split('-')[1] //penilaian
        // e.target.name.split('-')[0] //student
        switch (e.target.name.split('-')[1]) {
            case "aspek_penilaian_1":
                setResult(current => ({
                    ...current,
                    aspek_penilaian_1 : {
                        ...current.aspek_penilaian_1,[e.target.name.split('-')[0]] : parseInt(e.target.value)
                    }
                }))
                break;
            case "aspek_penilaian_2":
                setResult(current => ({
                    ...current,
                    aspek_penilaian_2 : {
                        ...current.aspek_penilaian_2,[e.target.name.split('-')[0]] : parseInt(e.target.value)
                    }
                }))
                break;
            case "aspek_penilaian_3":
                setResult(current => ({
                    ...current,
                    aspek_penilaian_3 : {
                        ...current.aspek_penilaian_3,[e.target.name.split('-')[0]] : parseInt(e.target.value)
                    }
                }))
                break;
            case "aspek_penilaian_4":
                setResult(current => ({
                    ...current,
                    aspek_penilaian_4 : {
                        ...current.aspek_penilaian_4,[e.target.name.split('-')[0]] : parseInt(e.target.value)
                    }
                }))
                break;
            default:
                break;
        }

        setShowResult(false)
    },[])
    
    return (    
        <div className='container'>
            <h2>Aplikasi Penilaian Mahasiswa</h2>
            <form onSubmit={handleOnSubmit}>
                <table cellPadding={5} cellSpacing={5} className='table'>
                    <thead>
                        <tr>
                            <th></th>
                            <th>Aspek Penilaian 1</th>
                            <th>Aspek Penilaian 2</th>
                            <th>Aspek Penilaian 3</th>
                            <th>Aspek Penilaian 4</th>
                        </tr>
                    </thead>
                    <tbody>
                        {studentsName.map((value, index) => (
                            <tr key={index}>
                                <td key={value+"-"+index}>Mahasiswa {value}</td>
                                <td><MemoizedForm name={`mahasiswa_${value}-aspek_penilaian_1`} fullwidth onChange={handleOnchangeValueForm}/></td>
                                <td><MemoizedForm name={`mahasiswa_${value}-aspek_penilaian_2`} fullwidth onChange={handleOnchangeValueForm}/></td>
                                <td><MemoizedForm name={`mahasiswa_${value}-aspek_penilaian_3`} fullwidth onChange={handleOnchangeValueForm}/></td>
                                <td><MemoizedForm name={`mahasiswa_${value}-aspek_penilaian_4`} fullwidth onChange={handleOnchangeValueForm}/></td>
                            </tr>
                        ))}
                    </tbody>
                </table>
                <button type="submit" className='button'>simpan</button>
            </form>
            
            {showResult && 
                <div style={{wordWrap: "break-word", marginTop : "20px"}}>
                    <pre>{JSON.stringify(result, null, 4)}</pre>
                </div>
            }
        </div>
    )
}

export default Student
