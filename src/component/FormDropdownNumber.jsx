import React from 'react'
import './style.css'

const FormDropdownNumber = ({name, fullwidth = false, onChange}) => {
    
    return (
        <div>
            {console.log(name)}
            <select name={name} className={fullwidth ? 'fullwidth' : ''} onChange={onChange}>
                {[1,2,3,4,5,6,7,8,9,10].map(value =>(
                    <option value={value} key={value}>{value}</option>
                ))}
            </select>
        </div>
    )
}

export default FormDropdownNumber
